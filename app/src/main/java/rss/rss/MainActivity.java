package rss.rss;
//Imports

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;

import rss.broadcastReceiver.BatteryChargeReceiver;
import rss.commandServer.CommandFactory;
import rss.commandServer.CommandManager;
import rss.commandServer.CommandManagerImpl;
import rss.graphs.GraphActivity;
import rss.joystick.JoystickView;
import rss.joystick.JoystickViewListener;
import rss.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnTouchListener, JoystickViewListener, SensorEventListener {

    private static final String robotPort = "8080";
    private static final int server_port = 12345;
    private static final int DEFAULT_ZOOM_LEVEL = 100;
    static CommandManager commandManager = new CommandManagerImpl();
    private static String robotIpAddress = "192.100.1.1";
    private static boolean robot_moving = false;

    static {
        commandManager.registerCommand("FORWARD", CommandFactory.getForwardCommand());
        commandManager.registerCommand("BACK", CommandFactory.getBackCommand());
        commandManager.registerCommand("LEFT", CommandFactory.getLeftCommand());
        commandManager.registerCommand("RIGHT", CommandFactory.getRightCommand());
        commandManager.registerCommand("STOP", CommandFactory.getStopCommand());
        commandManager.registerCommand("UP", CommandFactory.getUpCommand());
        commandManager.registerCommand("DOWN", CommandFactory.getDownCommand());
        commandManager.registerCommand("HALT", CommandFactory.getHaltCommand());
        //Other commands could be added here!
    }

    private Sensor mySensor;
    private SensorManager SM;
    private PendingIntent pendingIntent;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Create Sensor Manager
        SM = (SensorManager) getSystemService(SENSOR_SERVICE);

        //Accelerometer_Sensor
        mySensor = SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        //Register Sensor Listener
        SM.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_GAME);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setupButtons();
        setupWebView();
        setupBatteryChargeBroadcastReceiver();
        setupSettingsListeners();

    }

    private void setupSettingsListeners() {
        SettingsActivity.setRobotPreferenceListener(value -> {
            Log.i(MainActivity.class.getName(), "Setting IP address to: " + value);
            robotIpAddress = value;
            setupWebView();
        });

        SettingsActivity.setBatterySettingsListener(value -> {
            Log.i(MainActivity.class.getName(), "Setting battery threshold to: " + value);
            BatteryChargeReceiver.setDefaultThresholdValue(value);
        });
    }

    private void setupBatteryChargeBroadcastReceiver() {
        Intent alarmIntent = new Intent(MainActivity.this, BatteryChargeReceiver.class);

        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        // RTC -> Alarm time in System.currentTimeMillis() (wall clock time in UTC).
        manager.setRepeating(AlarmManager.RTC,
                SystemClock.currentThreadTimeMillis(),
                60 * 1000,
                pendingIntent);
    }

    private void setupButtons()
    {
        int ScreenOrientation = ((WindowManager) this.getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation();

        findViewById(R.id.backButton).setOnTouchListener(this);

        findViewById(R.id.forwardButton).setOnTouchListener(this);
        findViewById(R.id.upButton).setOnTouchListener(this);
        findViewById(R.id.downButton).setOnTouchListener(this);
        if(ScreenOrientation==Surface.ROTATION_0|| ScreenOrientation==Surface.ROTATION_180) {
            findViewById(R.id.rightButton).setOnTouchListener(this);
            findViewById(R.id.leftButton).setOnTouchListener(this);
        }
    }

    // SETUP VIDEO FOR APP
    private void setupWebView() {
        webView = (WebView) findViewById(R.id.webView);
        webView.setInitialScale(DEFAULT_ZOOM_LEVEL);
        webView.post(() -> {
            int width = webView.getWidth();
            int height = webView.getHeight();
            //Local IP, Optionally global IP
            webView.loadUrl("http://" + robotIpAddress + ":" + robotPort + "/stream" + "?width=" + width + "&height=" + height);
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void buttonPressed(JoystickView.JoystickDirection direction) {
        Log.i(MainActivity.class.getName(), "[INFO] Moving in " + direction.toString() + " direction");

        switch (direction) {
            case BACK:
                commandManager.execute("BACK", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
            case LEFT:
                commandManager.execute("LEFT", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
            case RIGHT:
                commandManager.execute("RIGHT", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
            case FORWARD:
                commandManager.execute("FORWARD", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
            case UP:
                commandManager.execute("UP", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
            case DOWN:
                commandManager.execute("DOWN", null, robotIpAddress + " " + Integer.toString(server_port));
                break;
        }
    }

    @Override
    public void buttonReleased(JoystickView.JoystickDirection direction) {
        Log.i(MainActivity.class.getName(), "[INFO] Button released");
        commandManager.execute("STOP", null, robotIpAddress + " " + Integer.toString(server_port));
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       if (id == R.id.nav_temperature) {
            Log.i(MainActivity.class.getName(), "[INFO] Launching \"GraphActivity\" activity...");
            Intent i = new Intent(this, GraphActivity.class);
            startActivity(i);


        }
        else if (id == R.id.nav_home) {
        }

        else if (id == R.id.nav_halt) {
            commandManager.execute("HALT", null, robotIpAddress + " " + Integer.toString(server_port));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == findViewById(R.id.forwardButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.FORWARD);
            else
                buttonReleased(JoystickView.JoystickDirection.FORWARD);
        } else if (v == findViewById(R.id.backButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.BACK);
            else
                buttonReleased(JoystickView.JoystickDirection.BACK);
        } else if (v == findViewById(R.id.leftButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.LEFT);
            else
                buttonReleased(JoystickView.JoystickDirection.LEFT);
        } else if (v == findViewById(R.id.rightButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.RIGHT);
            else
                buttonReleased(JoystickView.JoystickDirection.RIGHT);

        } else if (v == findViewById(R.id.upButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.UP);

        } else if (v == findViewById(R.id.downButton)) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                buttonPressed(JoystickView.JoystickDirection.DOWN);
        }

        return false;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        int ScreenOrientation = ((WindowManager) this.getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (ScreenOrientation) {
            case Surface.ROTATION_0: //portrait
            {
                if (event.values[0] > 2 && !robot_moving)//left
                {
                    commandManager.execute("LEFT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[0] < -2 && !robot_moving)//left
                {
                    commandManager.execute("RIGHT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[0] <= 2 && event.values[0] >= -2 && robot_moving) {
                    commandManager.execute("STOP", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = false;
                }
                break;
            }
            case Surface.ROTATION_90: //landscape
            {
                if (event.values[1] > 2 && !robot_moving)//left
                {
                    commandManager.execute("LEFT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[1] < -2 && !robot_moving)//left
                {
                    commandManager.execute("RIGHT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[1] <= 2 && event.values[1] >= -2 && robot_moving) {
                    commandManager.execute("STOP", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = false;
                }
                break;
            }
            case Surface.ROTATION_180://reverse portrait
            {
                if (event.values[0] > 2 && !robot_moving)//left
                {
                    commandManager.execute("RIGHT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[0] < -2 && !robot_moving)//right
                {
                    commandManager.execute("LEFT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[0] <= 2 && event.values[0] >= -2 && robot_moving) {
                    commandManager.execute("STOP", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = false;
                }
                break;
            }
            default://reverse landscape
            {
                if (event.values[1] > 2 && !robot_moving)//left
                {
                    commandManager.execute("RIGHT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[1] < -2 && !robot_moving)//right
                {
                    commandManager.execute("LEFT", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = true;
                } else if (event.values[1] <= 2 && event.values[1] >= -2 && robot_moving) {
                    commandManager.execute("STOP", null, robotIpAddress + " " + Integer.toString(server_port));
                    robot_moving = false;
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}