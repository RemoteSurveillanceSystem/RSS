package rss.serviceRSS;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class temperatureReceiver extends IntentService {

    String temperature;
    int server_port;
    byte[] message;
    DatagramPacket p;
    DatagramSocket s;

    public temperatureReceiver() {
        super("temperatureReceiver");
        server_port = 5005;
        message = new byte[4];
        try {
            p = new DatagramPacket(message, message.length);
            s = new DatagramSocket(server_port);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent transfer = new Intent("rss.serviceRSS.temperatureReceiver");

        try {
            s.receive(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
        temperature = new String(message, 0, p.getLength());
        Log.d("UDP", "message:" + temperature);
        //s.close();
        transfer.putExtra("temp", temperature);
        LocalBroadcastManager.getInstance(this).sendBroadcast(transfer);
    }

}