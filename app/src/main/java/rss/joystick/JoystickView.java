package rss.joystick;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import rss.rss.R;

public class JoystickView extends RelativeLayout {

    private Button forwardButton;
    private Button backButton;
    private Button rightButton;
    private Button leftButton;
    private Button upButton;
    private Button downButton;
    private JoystickViewListener listener;
    public JoystickView(Context context) {
        super(context);
        init(context);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(getContext(), R.layout.content_main, this);

        forwardButton = (Button) findViewById(R.id.forwardButton);
        backButton = (Button) findViewById(R.id.backButton);
        rightButton = (Button) findViewById(R.id.rightButton);
        leftButton = (Button) findViewById(R.id.leftButton);
        upButton = (Button) findViewById(R.id.upButton);
        downButton = (Button) findViewById(R.id.downButton);

        forwardButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.FORWARD);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    listener.buttonReleased(JoystickDirection.FORWARD);
                }
                return true;
            }
        });

        backButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.BACK);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    listener.buttonReleased(JoystickDirection.BACK);
                }
                return true;
            }
        });

        rightButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.RIGHT);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    listener.buttonReleased(JoystickDirection.RIGHT);
                }
                return true;
            }
        });

        leftButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.LEFT);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    listener.buttonReleased(JoystickDirection.LEFT);
                }
                return true;
            }
        });

        upButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.UP);
                }
                return true;
            }
        });

        downButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listener == null)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.buttonPressed(JoystickDirection.DOWN);
                }
                return true;
            }
        });
    }

    public boolean hasListener() {
        return listener != null;
    }

    public void setListener(JoystickViewListener listener) {
        this.listener = listener;
    }

    public enum JoystickDirection {FORWARD, BACK, RIGHT, LEFT, UP, DOWN}

}
