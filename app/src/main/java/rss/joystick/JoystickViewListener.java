package rss.joystick;

public interface JoystickViewListener {
    void buttonPressed(JoystickView.JoystickDirection direction);
    void buttonReleased(JoystickView.JoystickDirection direction);
}
