package rss.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static java.lang.Double.parseDouble;

/**
 * Created by varun on 3/23/17.
 */

public class TemperatureBReceiver extends BroadcastReceiver {
    private static double d = 35, d_last = 35;

    @Override
    public void onReceive(Context context, Intent intent) {
        String temp = intent.getStringExtra("temp");
        Log.d("receiver", "Got message: " + temp);
        try {
            d = parseDouble(temp);
            d_last = d;
        } catch (Exception e) {
            e.printStackTrace();
            d = d_last;
        }

    }


    public double temp_in_double() {
        return d;
    }
}
