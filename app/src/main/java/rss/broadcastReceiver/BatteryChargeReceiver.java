package rss.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import rss.sensors.BatterySensor;

/**
 * Created by davide on 13/03/17.
 */
public class BatteryChargeReceiver extends BroadcastReceiver {

    private static int DEFAULT_THRESHOLD_VALUE = 100;

    public static void setDefaultThresholdValue(int value) {
        DEFAULT_THRESHOLD_VALUE = value;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int thresholdValue = intent.getIntExtra("THRESHOLD", DEFAULT_THRESHOLD_VALUE);

        double batteryCharge = new BatterySensor(context).getData();
        if (batteryCharge < thresholdValue) {
            String toastMessage = "Low Battery (" + batteryCharge + "%)";
            if (DEFAULT_THRESHOLD_VALUE == 20) {
                toastMessage = "Very " + toastMessage;
            }
            Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
        }
    }
}
