package rss.settings.listeners;

/**
 * Created by davide on 13/03/17.
 */

public interface BatterySettingsListener {
    void selectThresholdValue(int value);
}
