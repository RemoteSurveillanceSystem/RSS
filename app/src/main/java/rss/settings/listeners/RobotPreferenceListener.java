package rss.settings.listeners;

/**
 * Created by davide on 19/03/17.
 */

public interface RobotPreferenceListener {
    void changedIPAddress(String value);
}
