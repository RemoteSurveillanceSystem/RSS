package rss.settings.listeners;


public interface GraphSettingsListener {
    void selectedGraphSpeed(int value);
    void selectedGraphSize(int value);
}
