package rss.graphs;

public enum GraphUpdateSpeed {
    SLOW(2000), NORMAL(1000), FAST(600);

    private int value;

    GraphUpdateSpeed(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

}
