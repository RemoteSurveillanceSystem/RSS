package rss.graphs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import rss.commandServer.CommandManager;
import rss.commandServer.CommandManagerImpl;
import rss.rss.MainActivity;
import rss.rss.R;
import rss.sensors.LightSensor;
import rss.sensors.Sensor;
import rss.sensors.SensorType;
import rss.sensors.TemperatureSensor;

public class GraphActivity extends AppCompatActivity implements View.OnTouchListener , NavigationView.OnNavigationItemSelectedListener{


    private static final int server_port = 12345;
    private static final int GRAPH_X_AXIS_SIZE = 10;
    static CommandManager commandManager = new CommandManagerImpl();
    private static String robotIpAddress = "192.100.1.1";
    private final Handler mHandler = new Handler();
    /**
     * Last x value for the last data point
     */
    private double lastXValue = 0d;

    /**
     * The update speed of the graph
     */
    private GraphUpdateSpeed updateSpeed = GraphUpdateSpeed.NORMAL;

    private GraphView graph;
    private Runnable timer;
    private LineGraphSeries<DataPoint> data;

    /**
     * The sensor to be used to get the data to be plotted
     */
    private Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_graph);
        graph = (GraphView) findViewById(R.id.temp_graph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data = new LineGraphSeries<>();
        graph.addSeries(data);
        graph.setOnTouchListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setupGraphViewport();

        //Default sensor -> temperature
        sensor = new TemperatureSensor(this.getBaseContext());
    }

    private void setupGraphViewport() {
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinY(30);
        viewport.setMaxY(60);
        viewport.setMaxX(GRAPH_X_AXIS_SIZE);
        viewport.setScrollable(true);
        viewport.setScalable(true);
    }

    public void setSensor(SensorType type) {
        switch (type) {
            case LIGHT:
                sensor = new LightSensor();
                break;
            case TEMPERATURE:
                sensor = new TemperatureSensor(this.getBaseContext());
                break;
            case BATTERY:
                //Code
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    private void startTimer() {
        timer = new Runnable() {
            @Override
            public void run() {
                data.appendData(getFromSensor(), true, 50);
                mHandler.postDelayed(this, updateSpeed.value());
            }
        };
        mHandler.postDelayed(timer, 300);
    }

    private DataPoint getFromSensor() {
        lastXValue += 1d;                   // X
        double sensorValue = sensor.getData(); // Y
        return new DataPoint(lastXValue, sensorValue);
    }

    @Override
    public void onPause() {
        mHandler.removeCallbacks(timer);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.graph_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.fast_graph_speed_menu_item:
                updateSpeed = GraphUpdateSpeed.FAST;
                mHandler.removeCallbacks(timer);
                startTimer();
                return true;

            case R.id.normal_graph_speed_menu_item:
                updateSpeed = GraphUpdateSpeed.NORMAL;
                mHandler.removeCallbacks(timer);
                startTimer();
                return true;

            case R.id.slow_graph_speed_menu_item:
                updateSpeed = GraphUpdateSpeed.SLOW;
                mHandler.removeCallbacks(timer);
                startTimer();
                return true;

            case R.id.temperature_sensor_menu_item:
                if (sensor.type() != SensorType.TEMPERATURE) {
                    setSensor(SensorType.TEMPERATURE);
                    data.resetData(new DataPoint[]{});
                }
                return true;

            case R.id.light_sensor_menu_item:
                if (sensor.type() != SensorType.LIGHT) {
                    setSensor(SensorType.LIGHT);
                    data.resetData(new DataPoint[]{});
                }
                return true;

            default:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                /**
                 * Resume the data gathering from sensors
                 */
                startTimer();
                break;
            case MotionEvent.ACTION_DOWN:
                /**
                 * If user press on the graph, data gathering activity is stopped
                 */
                mHandler.removeCallbacks(timer);
                break;
        }

        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_temperature) {
        }
        else if (id == R.id.nav_home) {
            Log.i(GraphActivity.class.getName(), "[INFO] Launching \"MainActivity\" activity...");
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

        }

        else if (id == R.id.nav_halt) {
            commandManager.execute("HALT", null, robotIpAddress + " " + Integer.toString(server_port));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
