package rss.commandServer;

public interface CommandManager {

	String execute(String string, ClientContext context, String args);

	void registerCommand(String name, Command command);
}
