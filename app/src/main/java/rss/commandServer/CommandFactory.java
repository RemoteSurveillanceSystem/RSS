package rss.commandServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class CommandFactory {

    public static Command getBackCommand() {
        return getMoveCommand("b");
    }

    public static Command getForwardCommand() {
        return getMoveCommand("f");
    }

    public static Command getLeftCommand() {
        return getMoveCommand("l");
    }

    public static Command getRightCommand() {
        return getMoveCommand("r");
    }

    public static Command getStopCommand() {
        return getMoveCommand("s");
    }

    public static Command getUpCommand() {
        return getMoveCommand("u");
    }

    public static Command getDownCommand() {
        return getMoveCommand("d");
    }

    public static Command getHaltCommand() {
        return getMoveCommand("h");
    }

    private static Command getMoveCommand(String direction) {
        return (c, args) -> {
            String data = direction;
            String robotIpAddress = args.split(" ")[0];
            int server_port = Integer.parseInt(args.split(" ")[1]);
            new Thread(() -> {
                try {
                    DatagramSocket s = new DatagramSocket();
                    InetAddress local = InetAddress.getByName(robotIpAddress);
                    int msg_length = data.length();
                    byte[] message = data.getBytes();
                    DatagramPacket p = new DatagramPacket(message, msg_length, local, server_port);
                    s.send(p);
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
            return "";
        };
    }

}
