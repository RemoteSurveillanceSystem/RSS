package rss.commandServer;

import java.util.HashMap;

public class CommandManagerImpl implements CommandManager {

    final HashMap<String, Command> commands = new HashMap<>();

    @Override
    public void registerCommand(String name, Command command) {
        commands.put(name, command);
    }

    @Override
    public String execute(final String name, ClientContext context, String args) {

        Command command = commands.get(name);
        if (command != null) {
            return command.execute(context, args);
        }

        return ((Command) (context1, args1) -> "Unknown Command: " + name).execute(context, args);

    }

}
