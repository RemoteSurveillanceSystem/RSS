package rss.commandServer;

public interface Command {
	String execute(ClientContext clientContext, String args);
}
