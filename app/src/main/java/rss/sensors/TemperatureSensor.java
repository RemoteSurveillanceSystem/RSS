package rss.sensors;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Random;

import rss.broadcastReceiver.TemperatureBReceiver;
import rss.serviceRSS.temperatureReceiver;

public class TemperatureSensor implements Sensor {

    TemperatureBReceiver receiver;
    Context contextTemp;

    public TemperatureSensor(Context context) {
        contextTemp = context;
        receiver = new TemperatureBReceiver();
        IntentFilter filter = new IntentFilter("rss.serviceRSS.temperatureReceiver");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);

    }

    @Override
    public double getData() {
        Log.i(TemperatureSensor.class.getName(), "Getting temperature from robot.");
        Intent intent = new Intent(contextTemp, temperatureReceiver.class);
        contextTemp.startService(intent);
        double d = receiver.temp_in_double();
        return d;
    }

    @Override
    public int getData(String message) {
        Log.i(TemperatureSensor.class.getName(), "Getting temperature from robot.");
        return new Random().nextInt(10);
    }

    @Override
    public SensorType type() {
        return SensorType.TEMPERATURE;
    }
}
