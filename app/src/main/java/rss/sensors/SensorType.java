package rss.sensors;

public enum SensorType {
    TEMPERATURE,
    LIGHT,
    BATTERY
}
