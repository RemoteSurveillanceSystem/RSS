package rss.sensors;

public interface Sensor {
    double getData();

    int getData(String message);

    SensorType type();
}
