package rss.sensors;

import android.util.Log;

import java.util.Random;

//For future implementations

public class LightSensor implements Sensor {

    @Override
    public double getData() {
        Log.i(TemperatureSensor.class.getName(), "Getting light value from robot.");
        //CODE FOR COMMUNICATING TO ROBOT

        //RETURN THE LIGHT VALUE AS INT

        return new Random().nextInt(10);
    }

    @Override
    public int getData(String message) {
        Log.i(TemperatureSensor.class.getName(), "Getting light value from robot.");
        return new Random().nextInt(10);
    }

    @Override
    public SensorType type() {
        return SensorType.LIGHT;
    }
}
