package rss.sensors;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import java.security.InvalidParameterException;

public class BatterySensor implements Sensor {

    private Context context;

    public BatterySensor(Context context) {
        if (context == null) {
            throw new InvalidParameterException("Context cannot be null");
        }
        this.context = context;
    }

    @Override
    public double getData() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        Log.i(BatterySensor.class.getName(), "Battery: " + String.valueOf(status) + "%");
        return status;
    }

    @Override
    public int getData(String message) {
        return 0;
    }

    @Override
    public SensorType type() {
        return SensorType.BATTERY;
    }
}
