# Remote Surveillance System based on Raspberry Pi for Mobile Application


  <h2> 1. Introduction</h2>

The RSS Team is very pleased to  announce the release of the Remote Surveillance System (RSS) project.

For  detailed documentation please check the following link:  https://hackmd.io/s/S1evszEie

  <h2> 2. Remote Surveillance System (RSS): Global Architecture.</h2>

The RSS project is an open source project with GPL License for the surveillance with low cost and   comprehensive features.
As shown in the following figure, the  RSS project can be divided into two parts: Hardware and Software parts. 


![](https://i.imgur.com/a6iLalg.png=200x200)

Figure: The Global architecture of the Remote Surveillance System (RSS) project.

In fact, in order to communicate between these parts we have used the WIFI technology. Specifically we have exploited  the  User Datagram Protocol (UDP). Moreover we have exploited  the sockets in order to control the robot.

Regarding the hardware part, the project uses a 3 - 4 Wheeled Chasis mounted with Micro-controller, Sensors and IP Camera and self contained battery as the remote equipment. The Sensors and the Chassis would be controlled by Micro-controller which would receive commands over blue-tooth The Micro-controller would also be able to control the direction of the IP Camera. The Microelectronic  also sends data to the master on request for different sensor values such as (the temperature,the humidity). Moreover, the video feed of the IP Camera would be sent over Wifi for  live streaming. 

Regarding the software part,  we  have a Master which is an Android Application which  sends data to our robot on the go via Wifi to control its direction and the direction for the camera as well using simple touch buttons or using accelerometer. 
This part allows also  live stream the data from the IP Camera connected through Wifi.


<h2>  2. Installing RSS project </h2>

Building RSS project requires that you download: 

+ RSS Android code for Software part of Remote Surveillance System based on Raspberry Pi for Mobile App:
 https://gitlab.com/RemoteSurveillanceSystem/RSS

+ Raspberry Codes and scripts for hardware part of Remote Surveillance System based on Raspberry Pi:
 https://gitlab.com/RemoteSurveillanceSystem/Raspberry/commits/master 

+  Hardware Tools


  + Raspberry Pi 3
  + L298N Motor Driver IC
  + Voltage converter
  + Servo 
  + Raspberry pi camera module of 5 MP
  + Camera
  + 2 DC Motors 


 <h2> 3. Tools: Description </h2>

+ *Raspberry Pi 3 *:

The Raspberry Pi 3 is the third generation Raspberry Pi. We have used the Raspberry Model B since it is well useful for embedded projects, and projects which require very low power.
Moreover, the Raspberry Pi 3 replaced the Raspberry Pi 2 Model B in February 2016.

Features:

Compared to the Raspberry Pi 2 it has:
*	A 1.2GHz 64-bit quad-core ARMv8 CPU
*	802.11n Wireless LAN
*	Bluetooth 4.1
*	Bluetooth Low Energy (BLE)

Like the Pi 2, it also has:
*	1GB RAM
*	4 USB ports
*	40 GPIO pins
*	Full HDMI port
*	Ethernet port
*	Combined 3.5mm audio jack and composite video
*	Camera interface (CSI)
*	Display interface (DSI)
*	Micro SD card slot (now push-pull rather than push-push)
*	Video Core IV 3D graphics core

The Raspberry Pi 3 has an identical form factor to the previous Pi 2 (and Pi 1 Model B+) and has complete compatibility with Raspberry Pi 1 and 2.


![idd](https://i.imgur.com/hr2fbYt.png=100x100)

+ *L298N Motor Driver IC  *:

The L298N  is a high voltage, high current, dual full-bridge motor driver designed to accept standard TTL logic levels and drive inductive loads such as relays, solenoids, DC and stepping motors.
Comes in a staggered, 15-lead Multiwatt package. Check below for a breakout board.

Features:

* Operating supply voltage of up to 46V
* 4.5-7VDC logic supply voltage
* Total DC current of up to 4A
* Low saturation voltage
* Over-temperature protection
* Logical ‘0’ input up to 1.5V (high-noise immunity)

The l293d can drive small and quiet big motors as well.

![](https://i.imgur.com/qVifFYz.png=2x1)


+ *Voltage converter :*

A voltage converter is an electric power converter which changes the voltage of an electrical power source. It may be combined with other components to create a power supply.
![](https://i.imgur.com/Np7lkN1.png)

+ *Servo :*

The Servo is used to change camera orientation

![](https://i.imgur.com/KhZvYTy.png)

+ *Raspberry pi camera module of 5 MP Description:*

The Raspberry Pi Camera Module is a 5 megapixel custom designed add-on for Raspberry Pi, featuring a fixed focus lens. It's capable of 2592 x 1944 pixel static images, and also supports 1080p30, 720p60 and 640x480p60/90 video. It attaches to Pi by way of one of the small sockets on the board upper surface and uses the dedicated CSi interface, designed especially for interfacing to cameras.

5 megapixel native resolution sensor-capable of 2592 x 1944 pixel static images
Supports 1080p30, 720p60 and 640x480p60/90 video
Camera is supported in the latest version of Raspbian, Raspberry Pi's preferred operating 
![idh](https://i.imgur.com/UggnduW.png=20x20)

+  *DC Motors* 

A DC motor is any of a class of rotary electrical machines that converts direct current electrical energy into mechanical energy. The most common types rely on the forces produced by magnetic fields.

The DC Motors used in our application to drive the robot. In our case we have used 2 DCs.
                
![id2](https://i.imgur.com/jda7F57.png=10x10)

<h2> 4. Conception</h2>

 **  class diagram **

![](https://i.imgur.com/IxUrcVy.png)




<h2> 5. Implementation  </h2>

+ <h2> Hardware Part </h2>

 + Monitor the Robot using the android application

![](https://i.imgur.com/hvCcWki.png)

+ RSS: access point:

![](https://i.imgur.com/zEvm5Xe.png)
 Figure:connection RSS

+ <h2> Software Part </h2>

     1) *Home Page*


The home page presents the launch screen which is the first screen and the entry point to the RSS  application. 
This screen gives the user a first impression of the app. Therefore, the launch screen is crucial especially for mobile application. We have implemented a  beautiful and stylish start screen.

![](https://i.imgur.com/gGavHKn.png)

2) Features

In order to facilitate the access to the different functionalities of application, the following figure shows the  menu that will appear when clicking on the button in top left of the screen.

This menu is constituted of the following elements:

     +1. Temperature: The temperature measured by the robot.
     +2. Accelerometer:
     +3. Safe Shutdown: Stop the robot.


![](https://i.imgur.com/OcAFZ33.png)


   3) *Setting:*

In order to configure the different functionalities of  our application (robot, graph..), we have  exploited a setting menu as shown in the following figure.  

![](https://i.imgur.com/JPfNg02.png)


   4) *Remote surveillance:*




![](https://i.imgur.com/6LJ5RpV.jpg)


<h2> 6. Problems  </h2>

The   problem is: 
+ How to connect Battery as 16.2 V to  components Raspberry and Servo which require 5 V.


Solution: 
+ As solution we have proposed to exploit the voltage convector as show in the following figure.

![](https://i.imgur.com/K2r04qA.png)


<h2> 7. Conclusion </h2>
 
For bugs, questions and discussions please use the Github Issues.
  
Contact us:
:::at: RSS_team@gmail.com :::

 <h1> **RSS Team Original Production ** </h1> 






