# Remote Surveillance System based on Raspberry Pi for Mobile App
 
 The RSS project is an open source project with GPL License for the surveillance with low cost and with more comprehensive features.

<h1>Constraints</h1>
The subject is free but nevertheless certain constraints are imposed:
<ul><li>The project will have to be developed in Java language using the standard Android library. It should be compatible with all devices running at least Android 4.0 (API 14 level).
</li><li>Any use of a library other than the standard Android library will require an explicitly approved request.
</li><li>The developed version must have a default interface in English; Optionally this one can be internationalized in French as well as in other languages ​​of its choice.
</li><li>The project will use several activities, at least one service and optionally a ContentProvider .
</li><li>The project will employ a minimum BroadcastReceiver dynamically registered with registerReceiverand at least one BroadcastReceiver declared in the manifest.
</li><li>The project will have to communicate with other machines (whether it is Bluetooth / WiFi communication with other Android devices or network communication with a server). It will be possible to develop in parallel if necessary a server that can be used by the Android application, this server can be realized with the language of its choice (language of which there is a layout and standard library free usable under Linux).
</li><li>The project will have at least one activity, the rendering of which can be adapted to the function of the Android terminal used (for example with a low-resolution version for phone and high-resolution tablet).
</li><li>The project will use at least one specific sensor available on a nomadic device (accelerometer, magnetometer, GPS for geolocation ...)
</li><li>The source code of the project will be offered as an open source license of choice and will be made public after the project development.
</li></ul>

<h1>Work organization</h1>
<ul><li>Each team is free to organize its work as it wishes. It is necessary that the workload is equitably distributed among the members. A good division of labor is one of the scoring criteria.
</li><li>A version management system (Git or Mercurial) should be used during development. You can self-host your source repository or use an external host as gitlab.com . It is best to keep your private repository as your defense has not occurred (for information, it is not possible to host a free private repository on GitHub.com ).
</li></ul>
<h1>Project Rendering</h1>
<ul><li>The rendering will be made either by calling a URL to retrieve a copy of your Git or Mercurial repository, or by sending an archive containing the deposit (by using the bundleGit or Mercurial for a file containing the deposit).
</li><li>The filing must contain a brief use of documentation in the form of a README and a more developed documentation in Markdown . This detailed documentation will include a development report with figures describing the architecture of the project, the organization of the work the difficulties encountered and the technical choices made. A LICENSE file at the root of the project will indicate the Open-Source license adopted for the project's development.
</li></ul>
<h1>Support</h1>
Each project will be defended in a free format of approximately 25 minutes. The defense will be mainly devoted to the demonstration for a quarter of an hour (if possible convincing) of the application. Questions may be asked during the meeting.

<h1>Dates of rendering and defense</h1>
<ul><li>The project must be returned no later than Tuesday, March 21, 2017 at 11:59 pm (Paris time) by submitting a file with the deposit or the URL of the deposit.
</li><li>The defense is organized for the morning of Friday, March 24, 2017 from 8:30 am. The schedules of passage will be communicated later according to the groups formed. If you need specific hardware (computers, tablets) for your demonstration, do not hesitate to ask for it at the time of rendering your project.
</li></ul>

<h1>Rating criteria</h1>
The main criteria that will be taken into account for scoring (their weighting has not yet been fixed):
<ul><li>Compliance with the instructions mentioned above. In particular, it is necessary to respect the constraints on the mandatory components of the application (activity, service, broadcast receiver ...).
</li><li>The originality and creativity of the project. The application should not be a simple reimplantation of an already existing application. However, it is tolerated to be based on existing applications, but a new approach or functionalities (representing a significant part of the work) must be introduced. It is advisable to browse through leading application stores (including the Google Play Store ) to ensure it does not already exist an application with significant similarities to the completed project.
</li><li>The difficulty of implementing the project. Projects that have been risk-taked will be rewarded even if their realization remains imperfect. In the same way, the volume of work related to the implementation of the project will be taken into account.
</li><li>The organization of teamwork. Tasks should be distributed equally among the different members of the work team. Automated checks may be applied to deposits.
</li><li>The quality of the code be it from a local point of view (code elegant, comments judicious) that global (good architecture ensuring the modularity of the components and the scalability of the application).
</li><li>The quality of the documentation provided (README, user documentation, development report).
</li><li>The correct operation of the application through the defense carried out (which includes a demonstration of the application which must be functional).
</li></ul>

