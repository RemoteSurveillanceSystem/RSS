
              1.       Remote Surveillance System (RSS) version 1.0


The RSS Team is very pleased to finally announce the release of version 1.0
of Remote Surveillance System  project.


				 1. 	Description

The RSS project is an open source project with GPL License for the surveillance with 
low cost and with more comprehensive features.
The  RSS project can be divided into 2 parts viz., Hardware and Software.
The project uses a 3 - 4 Wheeled Chasis mounted with Microcontroller, Sensors and IP
Camera and self contained battery as the remote equipment. 
The Sensors and the Chassis wouldbe controlled by Microcontroller which would receive 
commands over bluetooth. The Microcontroller would also be able to control the direction
of the IP Camera. The Microcontoller  also send data to the Master on request for
different sensor values. The video feed of the IP Camera would be sent over Wifi for 
live streaming.
On the other side,  we would have a Master which is an Android Application
which would send data to our robot on the go via bluetooth to control its
direction and the direction for the camera as well using simple touch buttons or
using accelerometer. It also live stream the data from the IP Camera connected through
Wifi.

Thanks to all involved!

The RSS Team

 1.                         Installing RSS

Building RSS project requires that you download the soucrce: 

https://gitlab.com/RemoteSurveillanceSystem/RSS
https://gitlab.com/RemoteSurveillanceSystem/Raspberry/commits/master

                1.          Tools

-Raspberry Pi 3
-L298N Motor Driver IC
-Voltage convertor
-Servo to change camera orientation
-Raspberry pi camera module of 5 MP
-Camera
-2 DC Motors to drive the robot

 1.                      Contact us

at: sss-project@gmail.com

For bugs, questions and discussions please use the Github Issues.
=========================================================================================
RSS Team Original Production 